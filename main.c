#include <stdint.h>
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"

#define THREAD_STACK_SIZE	100
#define THREAD1_PRIORITY	3
#define THREAD2_PRIORITY	4

volatile uint32_t shared_variable = 0;

static uint32_t thread1_stack[THREAD_STACK_SIZE],
			thread2_stack[THREAD_STACK_SIZE], idle_stack[THREAD_STACK_SIZE];

static StaticTask_t thread1_task_buf, thread2_task_buf;
static StaticTask_t	idle_task;

void os_sleep_ms(uint32_t ms)
{
	TickType_t xDelay = ms / portTICK_PERIOD_MS;

	vTaskDelay(xDelay);
}



void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer,
									StackType_t **ppxIdleTaskStackBuffer,
									uint32_t *pulIdleTaskStackSize )
{
	*ppxIdleTaskTCBBuffer	= &idle_task;
	*ppxIdleTaskStackBuffer	= idle_stack;
	*pulIdleTaskStackSize	= THREAD_STACK_SIZE;
}


void thread1(void *param_ptr)
{
	while (1) {
		shared_variable++;
		os_sleep_ms(10);
	}
}

void thread2(void *param_ptr)
{
	while (1) {
		shared_variable++;
		os_sleep_ms(10);
	}
}



int main()
{
	xTaskCreateStatic(thread1, "THD1", THREAD_STACK_SIZE,
						0, THREAD1_PRIORITY, thread1_stack, &thread1_task_buf);
	xTaskCreateStatic(thread2, "THD2", THREAD_STACK_SIZE,
						0, THREAD2_PRIORITY, thread2_stack, &thread2_task_buf);

	vTaskStartScheduler();
	return 1;
}
